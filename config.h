/* See LICENSE file for copyright and license details. */
// clang-format off

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx       = 1;  /* border pixel of windows */
static const unsigned int snap           = 32; /* snap pixel */
static const unsigned int systraypinning = 0;  /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;  /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;  /* systray spacing */
static const int systraypinningfailfirst = 1;  /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;  /* 0 means no systray */
static const int showbar                 = 1;  /* 0 means no bar */
static const int topbar                  = 1;  /* 0 means bottom bar */
static const char *fonts[]               = {"monospace:size=14"};
static const char dmenufont[]            = "monospace:size =14";
static const char col_gray1[]            = "#222222";
static const char col_gray2[]            = "#444444";
static const char col_gray3[]            = "#bbbbbb";
static const char col_gray4[]            = "#eeeeee";
static const char col_cyan[]             = "#005577";
static const char col_gray5[]            = "#5F819D";
static const char col_red1[]             = "#A54254";
static const char col_red2[]             = "#CC6666";
static const char col_green1[]           = "#B5BD68";

static const char *colors[][3] = {
    /*                                          fg         bg         border                       */
    [SchemeNorm]               =  {             col_gray3, col_gray1, col_gray2  },
    [SchemeSel]                =  {             col_gray4, col_cyan , col_cyan   },
    /*                                          text       bg          not used but cannot be empty */
    [SchemeStatus]             =  {             col_gray3, col_gray5,  "#000000"  }, // Statusbar right 
    [SchemeTagsSel]            =  {             col_gray4, col_red2,   "#000000"  }, // Tagbar left selected 
    [SchemeTagsNorm]           =  {             col_gray3, col_red1,   "#000000"  }, // Tagbar left unselected
    [SchemeInfoSel]            =  {             col_gray4, col_green1, "#000000"  }, // infobar middle selected 
    [SchemeInfoNorm]           =  {             col_gray3, col_green1, "#000000"  }, // infobar middle unselected 
};
/* tagging */
static const char *tags[] = {"Code", "Run",   "Doc", "Xreader", "Drawing",
                             "GC",   "Clion", "Web", "Music"};

static const Rule rules[] = {
    /* xprop(1):
     *	WM_CLASS(STRING) = instance, class
     *	WM_NAME(STRING) = title
     */
    /* class                          instance             title       tags mask     isfloating   monitor */
    {"Gimp",                          NULL,                NULL,       0,            1,           -1 },
    {"firefox",                       "Navigator",         NULL,       1 << 7,       0,           -1 },
    {"ASCII Table — Mozilla Firefox", NULL,                NULL,       1 << 8,       0,           -1 },
    {"okular",                        "okular",            NULL,       1 << 3,       0,           -1 },
    {"qtcreator",                     "QtCreator",         NULL,       1 << 6,       0,           -1 },
    {"Chromium",                      "chromium",          NULL,       1 << 8,       0,           -1 },
    {"jetbrains-clion",               NULL,                NULL,       1 << 6,       0,           -1 },
    {"kdevelop",                      NULL,                NULL,       1 << 6,       0,           -1 },
    {"Xreader",                       "xreader",           NULL,       1 << 3,       0,           -1 },
    {"OperationChariot",              NULL,                NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact        = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster        = 1;    /* number of clients in master area */
static const int resizehints    = 1; /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[]   = {
                                   /* symbol     arrange function */
                                   {"Tile",      tile              },/* first entry is default */
                                   {"Floating",  NULL              },/* no layout function means floating behavior */
                                   {"Monocle",   monocle           },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* Helpers */
static char       dmenumon[2]       = "0"; /* component of dmenucmd, manipulated in spawn() */

/* commands */
static const char *dmenucmd[]       = {"dmenu_run"                          , "-m"        , dmenumon    , "-fn"
                                                                            , dmenufont   , "-nb"       , col_gray1
                                                                            , "-nf"       , col_gray3   , "-sb"
                                                                            , col_cyan    , "-sf"       , col_gray4
                                                                                                                    , NULL};
static const char *termcmd[]        = {"uxterm"                                                                     , NULL};


/* static const char *kdevelop[]    = {"kdevelop"                                                                      , NULL}; */
/* static const char *start_gvim []    = {"projects/operation-chariot/start_vim.sh"                                    , NULL}; */
static const char *qt_assistant[]   = {"/usr/lib/qt6/bin/assistant"                                                 , NULL};
static const char *upvol[]          = {"/usr/local/bin/volume.sh"           , ".05+"                                , NULL};
static const char *downvol[]        = {"/usr/local/bin/volume.sh"           , ".05-"                                , NULL};
static const char *mutevol[]        = {"/usr/local/bin/volume.sh"           , "toggle"                              , NULL};
static const char *play_pause[]     = {"/usr/bin/playerctl"                 , "play-pause"                          , NULL};
static const char *play_next[]      = {"/usr/bin/playerctl"                 , "next"                                , NULL};
static const char *play_previous[]  = {"/usr/bin/playerctl"                 , "previous"                            , NULL};
static const char *feh_screen[]     = {"/home/mbenton/.fehbg"                                                       , NULL};
static const char *firefox[]        = {"firefox"                                                                    , NULL};
static const char *set_1_monitor[]  = {"/home/mbenton/.screenlayout/1mon.sh"                                        , NULL};
static const char *set_3_monitors[] = {"/home/mbenton/.screenlayout/3mon.sh"                                        , NULL};
static const char *set_2_monitors[] = {"/home/mbenton/.screenlayout/2mon.sh"                                         , NULL};
static const char *brightup[]       = {"xbacklight"                         , "-inc"      , "10"                    , NULL};
static const char *brightdown[]     = {"xbacklight"                         , "-dec"      , "10"                    , NULL};
static const char *lock[]           = {"slock"                                                                      , NULL};
static const char *xreader[]        = {"xreader"                                                                    , NULL};
static const char *screen_shot[]    = {"maim"                               , "-s"        , "/home/mbenton/temp.jpg", NULL};
static const char *clion_ide[]      = {"/home/mbenton/.local/share/JetBrains/clion/bin/clion"                       , NULL};
static Key keys[] = {
    /* modifier           key                       function               argument */
    {0                  , XF86XK_AudioLowerVolume , spawn          , {.v = downvol       }},
    {0                  , XF86XK_AudioMute        , spawn          , {.v = mutevol       }},
    {0                  , XF86XK_AudioRaiseVolume , spawn          , {.v = upvol         }},
    {0                  , XF86XK_AudioPlay        , spawn          , {.v = play_pause    }},
    {0                  , XF86XK_AudioNext        , spawn          , {.v = play_next     }},
    {0                  , XF86XK_AudioPrev        , spawn          , {.v = play_previous }},
    {0                  , XF86XK_MonBrightnessUp  , spawn          , {.v = brightup      }},
    {0                  , XF86XK_MonBrightnessDown, spawn          , {.v = brightdown    }},
    {MODKEY             , XK_Return               , spawn          , {.v = termcmd       }},
    {MODKEY | ShiftMask , XK_Return               , zoom           , {0                  }},
    {MODKEY             , XK_Tab                  , view           , {0                  }},
    {MODKEY             , XK_space                , setlayout      , {0                  }},
    {MODKEY | ShiftMask , XK_space                , togglefloating , {0                  }},
    {MODKEY             , XK_comma                , focusmon       , {.i = -1            }},
    {MODKEY             , XK_period               , focusmon       , {.i = +1            }},
    {MODKEY | ShiftMask , XK_comma                , tagmon         , {.i = -1            }},
    {MODKEY | ShiftMask , XK_period               , tagmon         , {.i = +1            }},
    {MODKEY             , XK_0                    , view           , {.ui = ~0           }},
    {MODKEY | ShiftMask , XK_0                    , tag            , {.ui = ~0           }},
    {MODKEY | ShiftMask , XK_a                    , spawn          , {.v = qt_assistant  }},
    {MODKEY             , XK_a                    , spawn          , {.v = xreader       }},
    {MODKEY             , XK_b                    , togglebar      , {0                  }},
    {MODKEY | ShiftMask , XK_c                    , quit           , {0                  }},
    {MODKEY             , XK_d                    , incnmaster     , {.i = -1            }},
    {MODKEY             , XK_f                    , spawn          , {.v = firefox       }},
    {MODKEY | ShiftMask , XK_f                    , setlayout      , {.v = &layouts[1]   }},
    {MODKEY | ShiftMask , XK_h                    , setmfact       , {.f = -0.05         }},
    {MODKEY             , XK_i                    , incnmaster     , {.i = +1            }},
    {MODKEY             , XK_h                    , focusstack     , {.i = +1            }},
    {MODKEY             , XK_l                    , focusstack     , {.i = -1            }},
    {MODKEY | ShiftMask , XK_l                    , spawn          , {.v = set_1_monitor }},
    {MODKEY | ShiftMask , XK_m                    , setlayout      , {.v = &layouts[2]   }},
    {MODKEY             , XK_n                    , spawn          , {.v = clion_ide     }},
    {MODKEY             , XK_o                    , spawn          , {.v = set_2_monitors}},
    {MODKEY | ShiftMask , XK_o                    , spawn          , {.v = set_3_monitors}},
    {MODKEY             , XK_p                    , spawn          , {.v = dmenucmd      }},
    {MODKEY | ShiftMask , XK_p                    , spawn          , {.v = screen_shot   }},
    {MODKEY | ShiftMask , XK_q                    , killclient     , {0                  }},
    {MODKEY | ShiftMask , XK_r                    , spawn          , {.v = feh_screen    }},
    {MODKEY             , XK_s                    , setmfact       , {.f = +0.05         }},
    {MODKEY | ShiftMask , XK_s                    , spawn          , {.v = lock          }},
    {MODKEY | ShiftMask , XK_t                    , setlayout      , {.v = &layouts[0]   }},
    TAGKEYS(XK_1        , 0) 
    TAGKEYS(XK_2        , 1) 
    TAGKEYS(XK_3        , 2) 
    TAGKEYS(XK_4        , 3)
    TAGKEYS(XK_5        , 4) 
    TAGKEYS(XK_6        , 5) 
    TAGKEYS(XK_7        , 6) 
    TAGKEYS(XK_8        , 7)
    TAGKEYS(XK_9        , 8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle   , * ClkClientWin          , or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function argument */
    {ClkTagBar           , MODKEY     , Button1      , tag           , {0}}           ,
    {ClkTagBar           , MODKEY     , Button3      , toggletag     , {0}}           ,
    {ClkWinTitle         , 0          , Button2      , zoom          , {0}}           ,
    {ClkStatusText       , 0          , Button2      , spawn         , {.v = termcmd}},
    {ClkClientWin        , MODKEY     , Button1      , movemouse     , {0}}           ,
    {ClkClientWin        , MODKEY     , Button2      , togglefloating, {0}}           ,
    {ClkClientWin        , MODKEY     , Button3      , resizemouse   , {0}}           ,
    {ClkTagBar           , 0          , Button1      , view          , {0}}           ,
    {ClkTagBar           , 0          , Button3      , toggleview    , {0}}           ,
    {ClkTagBar           , MODKEY     , Button1      , tag           , {0}}           ,
    {ClkTagBar           , MODKEY     , Button3      , toggletag     , {0}}           ,
};

#endif // CONFIG_H_INCLUDED
